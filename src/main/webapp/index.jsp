<!DOCTYPE html>
<%@page import="com.trivore.oneportal.example.openid.LogoutServlet"%>
<%@page import="java.net.URI"%>
<%@page import="javax.ws.rs.core.UriBuilder"%>
<%@page import="java.time.Instant"%>
<%@page import="com.trivore.oneportal.example.openid.SSOToken"%>
<%@page import="com.trivore.oneportal.example.openid.AuthFlowServlet"%>
<%@page import="com.trivore.oneportal.example.openid.OpenIdSession"%>
<%@page import="com.trivore.oneportal.example.openid.OpenIdUser"%>
<%@page import="java.util.Enumeration"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String title = "OpenID Connect example";
	OpenIdUser user = OpenIdSession.getUser(session);
	if(user == null) {
		SSOToken token = OpenIdSession.getSsoToken(session);
		if(token != null) {
			Instant validTo = token.getValidTo();
			if(validTo.isBefore(Instant.now())) {
				OpenIdSession.removeSsoToken(session);
				System.out.printf("SSO token has expired: token=%s, validTo=%s\n", token.getToken(), validTo);
			} else {
				URI authUrl = AuthFlowServlet.createURI(token.getToken());
				response.sendRedirect(authUrl.toString());
			}
		}
	}
%>
<html>
<head>
<title><%=title%></title>
</head>
<body>
<h3><%=title%></h3>
<% if(session == null || user == null) { %>
	<p>You are not signed in</p>
	<a href="<%= AuthFlowServlet.createURI().toString() %>">Sign-in via OpenID</a>
<% } else { %>
	<p>You are signed in as <%=user %></p>
	<a href="<%= LogoutServlet.createURI(true).toString() %>">Sign-out</a>
<% } %>
</body>
</html>
