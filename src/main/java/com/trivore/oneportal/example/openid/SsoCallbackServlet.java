package com.trivore.oneportal.example.openid;

import java.io.OutputStream;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(SsoCallbackServlet.PATH)
public class SsoCallbackServlet extends HttpServlet {
	
	public static final String PATH = "/sso-callback";
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		SSOToken ssoToken = SSOToken.createFrom(request);
		System.out.printf("Received OpenID SSO request: token=%s, validity=%s\n", ssoToken.getToken(), ssoToken.getValidTo());
		
		HttpSession session = request.getSession();
		
		SSOToken prevToken = OpenIdSession.getSsoToken(session);
		if (prevToken != null && prevToken.getToken().equals(ssoToken.getToken())) {
			/* Received same token again, do nothing. */
			System.out.println("SSO callback ignored: same SSO token received again: " + ssoToken.getToken());
		} else {
			/* Remove previous values*/
			OpenIdSession.removeUser(session);
			OpenIdSession.removeState(session);
			OpenIdSession.removeSsoToken(session);
			
			System.out.println("Removed all session attributes");
			
			/* Save new SSO token*/
			OpenIdSession.setSsoToken(session, ssoToken);
			System.out.println("Setup OpenID SSO token: " + ssoToken.getToken());
		}
		
		sendIcon(response);
	}
	
	/**
	 * Send 16x16 px icon as response. This icon may be shown
	 * on user's browser when signing in to this service.
	 * 
	 * @param response Http servlet response
	 */
	public static void sendIcon(HttpServletResponse response) {
		byte[] icon = Resources.getIcon();
		response.setStatus(HttpServletResponse.SC_OK);
		response.setContentType("image/png");
		response.setContentLength(icon.length);
		try (OutputStream out = response.getOutputStream()) {
			out.write(icon);
		} catch (Exception e) {
			System.out.println("Failed to write icon response");
			e.printStackTrace();
		}
	}
}
