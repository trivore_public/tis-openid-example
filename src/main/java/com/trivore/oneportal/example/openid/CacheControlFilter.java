package com.trivore.oneportal.example.openid;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

@WebFilter("/*")
public class CacheControlFilter implements Filter {
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
	}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (response instanceof HttpServletResponse) {
			HttpServletResponse httpResp = (HttpServletResponse) response;
			httpResp.setHeader("Cache-Control", "no-cache, no-store");
			httpResp.setHeader("Pragma", "no-cache");
		}
		chain.doFilter(request, response);
	}
	
	@Override
	public void destroy() {
		
	}
}
