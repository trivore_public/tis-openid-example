package com.trivore.oneportal.example.openid;

import javax.json.JsonObject;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(CallbackServlet.PATH)
public class CallbackServlet extends HttpServlet {
	
	public static final String PATH = "/callback";
	
	private static ServletContext ctx;
	
	@Override
	public void init(ServletConfig config) {
		ctx = config.getServletContext();
	}
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		
		String code = request.getParameter("code");
		String state = request.getParameter("state");
		// String scope = request.getParameter("scope");
		
		String error = request.getParameter("error");
		String errorDescription = request.getParameter("error_description");
		
		if (error != null) {
			System.out.printf("Received OpenID error: %s: %s\n", error, errorDescription);
			OpenIdSession.removeUser(session);
			OpenIdSession.removeSsoToken(session);
			OpenIdSession.removeState(session);
			try {
				response.sendRedirect(ctx.getContextPath());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return;
		}
		
		if (code == null || code.isEmpty()) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().println("Missing OpenID code!");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return;
		}
		
		String expectedState = OpenIdSession.getState(session);
		if (expectedState == null || !expectedState.equals(state)) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().printf("Invalid OpenID state: %s != %s\n", state, expectedState);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return;
		}
		
		try {
			OpenIdClient client = OpenIdManager.getClient();
			JsonObject tokenResp = client.codeTokenRequest(code);
			System.out.println(tokenResp);
			String idToken = tokenResp.getString("id_token");
			JsonObject userInfo = client.userInfo(tokenResp.getString("access_token"));
			System.out.println(userInfo);
			OpenIdUser user = new OpenIdUser();
			user.setSub(userInfo.getString("sub"));
			user.setUsername(userInfo.getString("preferred_username"));
			user.setFirstName(userInfo.getString("given_name", null));
			user.setLastName(userInfo.getString("family_name", null));
			user.setEmail(userInfo.getString("email", null));
			user.setMobile(userInfo.getString("mobile", null));
			user.setIdToken(idToken);
			OpenIdSession.setUser(session, user);
		} catch (Exception e) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().println(e.getMessage());
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			e.printStackTrace();
		}
		
		try {
			response.sendRedirect(ctx.getContextPath());
		} catch (Exception e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}
}
