package com.trivore.oneportal.example.openid;

import java.io.Serializable;
import java.time.Duration;
import java.time.Instant;

import javax.servlet.http.HttpServletRequest;

public class SSOToken implements Serializable {
	
	public static final long serialVersionUID = 1;
	
	private String token;
	private Instant validTo;
	
	public SSOToken() {
		super();
	}
	
	public SSOToken(String token, Instant validTo) {
		this();
		setToken(token);
		setValidTo(validTo);
	}
	
	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public Instant getValidTo() {
		return validTo;
	}
	
	public void setValidTo(Instant validTo) {
		this.validTo = validTo;
	}
	
	public static SSOToken createFrom(HttpServletRequest request) {
		String token = request.getParameter("sso-token");
		String strValidity = request.getParameter("sso-validity");
		return new SSOToken(token, Instant.now().plus(Duration.ofMinutes(Integer.parseInt(strValidity))));
	}
}
