package com.trivore.oneportal.example.openid;

import java.net.URI;
import java.util.UUID;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.UriBuilder;

@WebServlet(value = AuthFlowServlet.PATH, loadOnStartup = 1)
public class AuthFlowServlet extends HttpServlet {
	
	public static final String PATH = "/auth-flow";
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		String state = UUID.randomUUID().toString();
		OpenIdSession.setState(session, state);
		System.out.println("Setup OpenID state: " + state);
		
		String ssoToken = request.getParameter("sso_token");
		
		URI authUri = OpenIdManager.getClient().createAuthURI(state, ssoToken);
		try {
			response.sendRedirect(authUri.toString());
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}
	
	public static URI createURI() {
		return createURI(null);
	}
	
	public static URI createURI(String ssoToken) {
		UriBuilder ub = OpenIdManager.buildURI(PATH);
		if (ssoToken != null) {
			ub.queryParam("sso_token", ssoToken);
		}
		return ub.build();
	}
}
