package com.trivore.oneportal.example.openid;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Resources {
	
	private static final String ICON_RES = "openid.png";
	
	private static byte[] icon;
	
	private Resources() {
		/* Static methods only */
	}
	
	static {
		/* Load 16x16 px icon from classpath resource. */
		ClassLoader cl = Resources.class.getClassLoader();
		try {
			InputStream iconInput = cl.getResourceAsStream(ICON_RES);
			ByteArrayOutputStream iconOutput = new ByteArrayOutputStream(iconInput.available());
			byte[] buf = new byte[4096];
			int len;
			while ((len = iconInput.read(buf)) > 0) {
				iconOutput.write(buf, 0, len);
			}
			icon = iconOutput.toByteArray();
		} catch (IOException e) {
			throw new IllegalStateException("Failed to load icon resource: " + ICON_RES, e);
		}
	}
	
	public static byte[] getIcon() {
		return icon;
	}
}
