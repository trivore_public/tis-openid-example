package com.trivore.oneportal.example.openid;

public class OpenIdException extends Exception {
	
	public OpenIdException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public OpenIdException(String message) {
		super(message);
	}
}
