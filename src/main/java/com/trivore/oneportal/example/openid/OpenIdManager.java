package com.trivore.oneportal.example.openid;

import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.UriBuilder;

@WebListener
public class OpenIdManager implements ServletContextListener {
	
	private static final String PROP_CONFIG_LOCATION = "openid.example.configLocation";
	private static final String PROP_METADATA_URL = "openid.example.metadataUrl";
	private static final String PROP_REDIRECT_URL = "openid.example.redirectUrl";
	private static final String PROP_CLIENT_ID = "openid.example.clientId";
	private static final String PROP_CLIENT_SECRET = "openid.example.clientSecret";
	private static final String PROP_SCOPE = "openid.example.scope";
	
	private static ServletContext ctx;
	
	private static OpenIdManager instance;
	
	private static OpenIdClient client;
	
	public OpenIdManager() {
		super();
	}
	
	/**
	 * Called by servlet container on webapp initialisation.
	 * 
	 * @param sce Servlet context event
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ctx = sce.getServletContext();
		
		/* Setup REST API client with basic authentication */
		
		Properties config = new Properties();
		
		String configLocation = System.getenv("OPENID_EXAMPLE_CONFIG_LOCATION");
		if (configLocation == null || configLocation.isEmpty()) {
			configLocation = getConfigProp(config, ctx, PROP_CONFIG_LOCATION);
		}
		
		if (configLocation != null && !configLocation.isEmpty()) {
			System.out.println("Loading configuration from " + configLocation);
			URL configURL;
			try {
				configURL = new URL(configLocation);
			} catch (Exception e) {
				IllegalArgumentException iae = new IllegalArgumentException(String.format("Invalid URL: %s=%s",
						PROP_CONFIG_LOCATION, configLocation), e);
				if (!configLocation.startsWith("file:")) {
					try {
						configURL = new URL("file:" + configLocation);
					} catch (Exception e2) {
						iae.printStackTrace();
						throw iae;
					}
				} else {
					iae.printStackTrace();
					throw iae;
				}
			}
			
			try (InputStream input = configURL.openStream()) {
				config.load(input);
			} catch (Exception e) {
				throw new IllegalArgumentException(String.format("Failed to load URL: %s=%s", PROP_CONFIG_LOCATION,
						configLocation), e);
			}
		}
		
		String metadataUrl = getConfigProp(config, ctx, PROP_METADATA_URL);
		String redirectUrl = getConfigProp(config, ctx, PROP_REDIRECT_URL);
		String clientId = getConfigProp(config, ctx, PROP_CLIENT_ID);
		String clientSecret = getConfigProp(config, ctx, PROP_CLIENT_SECRET);
		String scope = getConfigProp(config, ctx, PROP_SCOPE);
		
		System.out.println("Using Metadata URL:  " + metadataUrl);
		System.out.println("Using Redirect URL:  " + redirectUrl);
		System.out.println("Using Client ID:     " + clientId);
		System.out.println("Using Client secret: " + clientSecret);
		
		try {
			client = new OpenIdClient(new URI(metadataUrl), new URI(redirectUrl), clientId, clientSecret, scope);
			client.setup();
		} catch (URISyntaxException use) {
			use.printStackTrace();
			throw new IllegalArgumentException("Invalid metadata/redirect URL", use);
		} catch (Exception e) {
			e.printStackTrace();
			throw new IllegalArgumentException("Failed to setup OpenID client", e);
		}
		
		System.out.println("Initialized OpenID Connect manager");
		
		instance = this;
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}
	
	private String getConfigProp(Properties config, ServletContext ctx, String name) {
		String value = System.getProperty(name);
		if (value == null || value.isEmpty()) {
			value = config.getProperty(name);
		}
		if (value == null || value.isEmpty()) {
			value = ctx.getInitParameter(name);
		}
		if (value == null || value.isEmpty()) {
			throw new IllegalArgumentException("Missing required config property: " + name);
		}
		return value;
	}
	
	public static OpenIdUser getUser(HttpSession session) {
		OpenIdUser user = OpenIdSession.getUser(session);
		if (user != null) {
			return user;
		}
		return null;
	}
	
	public static UriBuilder buildURI(String path) {
		return UriBuilder.fromPath(getContextPath()).path(path);
	}
	
	public static String getContextPath() {
		return ctx.getContextPath();
	}
	
	public static OpenIdClient getClient() {
		return client;
	}
	
	public static OpenIdManager getInstance() {
		return instance;
	}
}
