package com.trivore.oneportal.example.openid;

import java.io.Serializable;

public class OpenIdUser implements Serializable {
	
	public static final long serialVersionUID = 1;
	
	private String sub;
	private String username;
	private String firstName;
	private String lastName;
	private String email;
	private String mobile;
	
	private String idToken;
	
	public OpenIdUser() {
		super();
	}
	
	public String getSub() {
		return sub;
	}
	
	public void setSub(String sub) {
		this.sub = sub;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getMobile() {
		return mobile;
	}
	
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	public String getIdToken() {
		return idToken;
	}
	
	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}
	
	private String formatName() {
		if (firstName != null) {
			if (lastName != null) {
				return firstName + " " + lastName;
			} else {
				return firstName;
			}
		} else if (lastName != null) {
			return lastName;
		}
		return null;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName()).append("(");
		sb.append(sub);
		sb.append(", ").append(username);
		String name = formatName();
		if (name != null) {
			sb.append(", name=").append(name);
		}
		if (email != null) {
			sb.append(", email=").append(email);
		}
		if (mobile != null) {
			sb.append(", mobile=").append(mobile);
		}
		sb.append(")");
		return sb.toString();
	}
}
