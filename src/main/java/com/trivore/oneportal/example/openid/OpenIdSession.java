package com.trivore.oneportal.example.openid;

import javax.servlet.http.HttpSession;

public class OpenIdSession {
	
	/* OpenID session attribute names */
	public static final String ATTR_USER = "openid-user";
	public static final String ATTR_STATE = "openid-state";
	public static final String ATTR_SSO_TOKEN = "openid-sso-token";
	
	public static OpenIdUser getUser(HttpSession session) {
		if (session == null) {
			return null;
		}
		Object user = session.getAttribute(ATTR_USER);
		if (user == null) {
			return null;
		} else if (user instanceof OpenIdUser) {
			return (OpenIdUser) user;
		} else {
			throw new IllegalArgumentException("Invalid OpenID user object: " + user);
		}
	}
	
	public static void setUser(HttpSession session, OpenIdUser user) {
		session.setAttribute(ATTR_USER, user);
	}
	
	public static void removeUser(HttpSession session) {
		session.removeAttribute(ATTR_USER);
	}
	
	public static String getState(HttpSession session) {
		return (String) session.getAttribute(ATTR_STATE);
	}
	
	public static void setState(HttpSession session, String state) {
		session.setAttribute(ATTR_STATE, state);
	}
	
	public static void removeState(HttpSession session) {
		session.removeAttribute(ATTR_STATE);
	}
	
	public static SSOToken getSsoToken(HttpSession session) {
		return (SSOToken) session.getAttribute(ATTR_SSO_TOKEN);
	}
	
	public static void setSsoToken(HttpSession session, SSOToken ssoToken) {
		session.setAttribute(ATTR_SSO_TOKEN, ssoToken);
	}
	
	public static void removeSsoToken(HttpSession session) {
		session.removeAttribute(ATTR_SSO_TOKEN);
	}
	
	public static void clearSession(HttpSession session) {
		removeUser(session);
		removeState(session);
		removeSsoToken(session);
	}
}
