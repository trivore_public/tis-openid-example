package com.trivore.oneportal.example.openid;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.core.UriBuilder;

public class OpenIdClient {
	
	private static final Base64.Encoder ENC = Base64.getEncoder();
	
	private final URI metadataEndpoint;
	private final URI redirectUrl;
	private final String clientId;
	private final String clientSecret;
	private final String scope;
	
	private URI authEndpoint;
	private URI tokenEndpoint;
	private URI userInfoEndpoint;
	private URI endSessionEndpoint;
	
	private List<String> supportedScopes;
	private List<String> supportedClaims;
	
	public OpenIdClient(URI metadataEndpoint, URI redirectUrl, String clientId, String clientSecret, String scope) {
		super();
		this.metadataEndpoint = metadataEndpoint;
		this.redirectUrl = redirectUrl;
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.scope = scope;
	}
	
	public void setup() throws OpenIdException {
		try (InputStream metadataInput = metadataEndpoint.toURL().openStream();
				JsonReader metadataReader = Json.createReader(metadataInput)) {
			JsonObject metadata = metadataReader.readObject();
			
			authEndpoint = new URI(metadata.getString("authorization_endpoint"));
			tokenEndpoint = new URI(metadata.getString("token_endpoint"));
			userInfoEndpoint = new URI(metadata.getString("userinfo_endpoint"));
			if (metadata.containsKey("end_session_endpoint")) {
				endSessionEndpoint = new URI(metadata.getString("end_session_endpoint"));
			}
			
			supportedScopes = new ArrayList<>();
			JsonArray jsonScopes = metadata.getJsonArray("scopes_supported");
			for (int i = 0; i < jsonScopes.size(); i++) {
				supportedScopes.add(jsonScopes.getString(i));
			}
			
			supportedClaims = new ArrayList<>();
			JsonArray jsonClaims = metadata.getJsonArray("claims_supported");
			for (int i = 0; i < jsonClaims.size(); i++) {
				supportedClaims.add(jsonClaims.getString(i));
			}
			
			System.out.println("Authorization endpoint: " + authEndpoint);
			System.out.println("Token endpoint:         " + tokenEndpoint);
			System.out.println("User info endpoint:     " + userInfoEndpoint);
			System.out.println("End session endpoint:   " + endSessionEndpoint);
			
			for (String scope : supportedScopes) {
				System.out.println("Supported scope: " + scope);
			}
			
			for (String claim : supportedClaims) {
				System.out.println("Supported claim: " + claim);
			}
		} catch (Exception e) {
			throw new OpenIdException("Failed to fetch OpenID metadata", e);
		}
	}
	
	public List<String> getSupportedScopes() {
		return supportedScopes;
	}
	
	public List<String> getSupportedClaims() {
		return supportedClaims;
	}
	
	public URI createAuthURI(String state, String ssoToken) {
		UriBuilder ub = UriBuilder.fromUri(authEndpoint);
		ub.queryParam("response_type", "code");
		ub.queryParam("scope", scope);
		ub.queryParam("client_id", clientId);
		ub.queryParam("redirect_uri", redirectUrl);
		ub.queryParam("state", state);
		if (ssoToken != null) {
			ub.queryParam("sso_token", ssoToken);
			ub.queryParam("prompt", "none");
		}
		return ub.build();
	}
	
	public JsonObject codeTokenRequest(String code) throws OpenIdException {
		HttpURLConnection con = null;
		OutputStream out = null;
		InputStream in = null;
		try {
			StringBuilder data = new StringBuilder();
			data.append("grant_type=authorization_code");
			data.append("&code=").append(URLEncoder.encode(code, "UTF-8"));
			data.append("&redirect_uri=").append(URLEncoder.encode(redirectUrl.toString(), "UTF-8"));
			
			byte[] auth = (clientId + ":" + clientSecret).getBytes(StandardCharsets.UTF_8);
			String authHeader = "Basic " + ENC.encodeToString(auth);
			con = (HttpURLConnection) tokenEndpoint.toURL().openConnection();
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			con.setRequestProperty("Authorization", authHeader);
			con.connect();
			
			out = con.getOutputStream();
			out.write(data.toString().getBytes(StandardCharsets.UTF_8));
			out.flush();
			
			int status = con.getResponseCode();
			if (status != 200) {
				throw new OpenIdException("Failed to perform OpenID token request: status=" + status);
			}
			
			in = con.getInputStream();
			try (JsonReader reader = Json.createReader(in)) {
				return reader.readObject();
			}
		} catch (IOException ioe) {
			throw new OpenIdException("Failed to perform OpenID token request", ioe);
		} finally {
			if (con != null) {
				con.disconnect();
			}
			close(out);
			close(in);
		}
	}
	
	public JsonObject userInfo(String accessToken) throws OpenIdException {
		HttpURLConnection con = null;
		InputStream in = null;
		try {
			con = (HttpURLConnection) userInfoEndpoint.toURL().openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("Authorization", "Bearer " + accessToken);
			con.connect();
			
			int status = con.getResponseCode();
			if (status != 200) {
				throw new OpenIdException("Failed to perform OpenID userInfo request: status=" + status);
			}
			
			in = con.getInputStream();
			try (JsonReader reader = Json.createReader(in)) {
				return reader.readObject();
			}
		} catch (IOException ioe) {
			throw new OpenIdException("Failed to perform OpenID userInfo request", ioe);
		} finally {
			if (con != null) {
				con.disconnect();
			}
			close(in);
		}
	}
	
	public URI endSession(URI postLogoutUri, String idToken) {
		if (endSessionEndpoint == null) {
			return null;
		}
		UriBuilder ub = UriBuilder.fromUri(endSessionEndpoint);
		ub = ub.queryParam("post_logout_redirect_uri", postLogoutUri);
		if (idToken != null) {
			ub = ub.queryParam("id_token_hint", idToken);
		}
		return ub.build();
	}
	
	private void close(Closeable c) {
		if (c == null) {
			return;
		}
		try {
			c.close();
		} catch (Exception e) {
			// Ignore
		}
	}
}
