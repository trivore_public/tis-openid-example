package com.trivore.oneportal.example.openid;

import java.net.URI;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.UriBuilder;

@WebServlet(LogoutServlet.PATH)
public class LogoutServlet extends HttpServlet {
	
	public static final String PATH = "/logout";
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("Received OpenID logout request");
		
		boolean interactive = Boolean.parseBoolean(request.getParameter("interactive"));
		
		OpenIdUser user = null;
		HttpSession session = request.getSession(false);
		if (session != null) {
			user = OpenIdSession.getUser(session);
			OpenIdSession.clearSession(session);
		}
		
		UriBuilder ub = UriBuilder.fromUri(request.getRequestURL().toString());
		ub.replacePath(OpenIdManager.getContextPath());
		URI postLogout = ub.build();
		
		String idToken = user != null ? user.getIdToken() : null;
		URI endSession = OpenIdManager.getClient().endSession(postLogout, idToken);
		
		try {
			if (interactive) {
				/* Redirect user if this interactive browser session and not an sso logout request. */
				if (endSession != null) {
					response.sendRedirect(endSession.toString());
				} else {
					response.sendRedirect(postLogout.toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static URI createURI(boolean logoutOp) {
		return OpenIdManager.buildURI(PATH).queryParam("interactive", logoutOp).build();
	}
}
